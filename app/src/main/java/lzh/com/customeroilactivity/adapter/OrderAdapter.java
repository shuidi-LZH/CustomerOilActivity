package lzh.com.customeroilactivity.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import lzh.com.customeroilactivity.Bean.BespeakRefual;
import lzh.com.customeroilactivity.R;

/**
 * Created by LuoZhihao on 2018/11/2.
 */
public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    private List<BespeakRefual> mList;
    private String objectId;

    static class ViewHolder extends RecyclerView.ViewHolder{

        TextView orderId;
        TextView oilType;
        TextView number;
        TextView time;
        TextView moneyState;
        TextView money;
        TextView allState;
        View adapterView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            adapterView = itemView;
            orderId = (TextView)itemView.findViewById(R.id.order_id);
            oilType = (TextView)itemView.findViewById(R.id.oil_type);
            number = (TextView)itemView.findViewById(R.id.number);
            time = (TextView)itemView.findViewById(R.id.time);
            moneyState = (TextView)itemView.findViewById(R.id.money_state);
            money = (TextView)itemView.findViewById(R.id.money);
            allState = (TextView)itemView.findViewById(R.id.all_state);
        }
    }

    public OrderAdapter(List<BespeakRefual> list){
        mList= list;
    }

    @NonNull
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_order,viewGroup,false);
        ViewHolder holder = new ViewHolder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAdapter.ViewHolder viewHolder, int i) {
        BespeakRefual bespeakRefual = mList.get(i);
        viewHolder.orderId.setText(bespeakRefual.getBespeakRefualId());
        viewHolder.oilType.setText(bespeakRefual.getOilType());
        if (bespeakRefual.getQuantity().indexOf("升")<bespeakRefual.getQuantity().indexOf("元")){

            int num = bespeakRefual.getQuantity().indexOf("升");

            viewHolder.number.setText(bespeakRefual.getQuantity().substring(0,num+1));
            viewHolder.money.setText(bespeakRefual.getQuantity().substring(num+2,bespeakRefual.getQuantity().length()-1));
        }else if (bespeakRefual.getQuantity().indexOf("升")>bespeakRefual.getQuantity().indexOf("元")){

            int num = bespeakRefual.getQuantity().indexOf("元");

            viewHolder.money.setText(bespeakRefual.getQuantity().substring(0,num+1));
            viewHolder.number.setText(bespeakRefual.getQuantity().substring(num+2,bespeakRefual.getQuantity().length()-1));
        }


        viewHolder.moneyState.setText(bespeakRefual.getState()+"：");
        viewHolder.allState.setText(bespeakRefual.getState());
        viewHolder.time.setText(bespeakRefual.getTime());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
