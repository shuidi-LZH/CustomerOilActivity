package lzh.com.customeroilactivity.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.UpdateListener;
import lzh.com.customeroilactivity.Bean.Company;
import lzh.com.customeroilactivity.R;
import lzh.com.customeroilactivity.activity.CompanyActivity;

/**
 * Created by LuoZhihao on 2018/10/29.
 */
public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.ViewHolder> {

    private List<Company> mList;
    private AlertDialog alertDialog;
    private String objectId;

    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView name;
        View adapterView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            adapterView = itemView;
            name = (TextView)itemView.findViewById(R.id.name);
        }
    }

    public CompanyAdapter(List<Company> list){
        mList= list;
    }


    @NonNull
    @Override
    public CompanyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_company,viewGroup,false);
        ViewHolder holder = new ViewHolder(view);

        holder.adapterView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = holder.getAdapterPosition();
                Company company = mList.get(position);

                Intent intent = new Intent();
                intent = intent.setClass(view.getContext(),CompanyActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("name",company.getCompanyDetailName());
                intent.putExtras(bundle);
                CompanyActivity.companyActivity.setResult(1,intent);//RESULT_OK是返回状态码  
                CompanyActivity.companyActivity.finish();//会触发onDestroy(); 
            }
        });


        holder.adapterView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                View newView = LayoutInflater.from(view.getContext()).inflate(R.layout.out_dialog,null);
                TextView yes = (TextView)newView.findViewById(R.id.yes);
                TextView no = (TextView)newView.findViewById(R.id.no);
                TextView content = (TextView)newView.findViewById(R.id.content);
                content.setText("确认删除该公司？");

                AlertDialog dialog = new AlertDialog.Builder(view.getContext(),R.style.DialogStyle)
                        .setView(newView)
                        .create();
                dialog.show();
                alertDialog = dialog;

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {

                        Bmob.initialize(view.getContext(), "5e509d9bd6152b90a5e72d4cade54c5f");
                        int position = holder.getAdapterPosition();
                        Company company = mList.get(position);
                        objectId = company.getObjectId();

                        Company newCompany = new Company();
                        newCompany.setObjectId(objectId);
                        newCompany.delete(new UpdateListener() {

                            @Override
                            public void done(BmobException e) {
                                if(e==null){
                                    Toast.makeText(view.getContext(), "删除成功", Toast.LENGTH_SHORT).show();
                                    CompanyActivity.companyActivity.find();
                                }else{
                                    Toast.makeText(view.getContext(), "删除失败，请联系管理员", Toast.LENGTH_SHORT).show();
                                }
                            }

                        });
                        alertDialog.dismiss();
                    }
                });

                return true;
            }
        });


        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyAdapter.ViewHolder viewHolder, int i) {

        Company company = mList.get(i);
        viewHolder.name.setText(company.getCompanyDetailName());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
