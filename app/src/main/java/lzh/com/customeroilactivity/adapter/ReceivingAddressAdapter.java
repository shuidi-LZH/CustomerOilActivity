package lzh.com.customeroilactivity.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.UpdateListener;
import lzh.com.customeroilactivity.Bean.Address;
import lzh.com.customeroilactivity.R;
import lzh.com.customeroilactivity.activity.ReceivingAddressActivity;

/**
 * Created by LuoZhihao on 2018/10/26.
 */
public class ReceivingAddressAdapter extends RecyclerView.Adapter<ReceivingAddressAdapter.ViewHolder> {

    private List<Address> mList;
    private AlertDialog alertDialog;
    private String objectId;

    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView name;
        TextView iphone;
        TextView address;
        TextView detailAddress;
        View adapterView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            adapterView = itemView;
            name = (TextView)itemView.findViewById(R.id.name);
            iphone = (TextView)itemView.findViewById(R.id.iphone);
            address = (TextView)itemView.findViewById(R.id.address);
            detailAddress = (TextView)itemView.findViewById(R.id.detail_address);
        }
    }

    public ReceivingAddressAdapter(List<Address> list){
        mList= list;
    }


    @NonNull
    @Override
    public ReceivingAddressAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_receiving_address,viewGroup,false);
        final ViewHolder holder = new ViewHolder(view);
        holder.adapterView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = holder.getAdapterPosition();
                Address address = mList.get(position);

                Intent intent = new Intent();
                intent = intent.setClass(view.getContext(),ReceivingAddressActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("name",address.getName());
                bundle.putString("iphone",address.getIphone());
                bundle.putString("address",address.getAddress());
                bundle.putString("detailAddress",address.getDetailedAddress());
                intent.putExtras(bundle);
                ReceivingAddressActivity.receivingAddressActivity.setResult(0,intent);//RESULT_OK是返回状态码  
                ReceivingAddressActivity.receivingAddressActivity.finish();//会触发onDestroy(); 
            }
        });

        holder.adapterView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                View newView = LayoutInflater.from(view.getContext()).inflate(R.layout.out_dialog,null);
                TextView yes = (TextView)newView.findViewById(R.id.yes);
                TextView no = (TextView)newView.findViewById(R.id.no);
                TextView content = (TextView)newView.findViewById(R.id.content);
                content.setText("确认删除该地址？");

                AlertDialog dialog = new AlertDialog.Builder(view.getContext(),R.style.DialogStyle)
                        .setView(newView)
                        .create();
                dialog.show();
                alertDialog = dialog;

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {

                        Bmob.initialize(view.getContext(), "5e509d9bd6152b90a5e72d4cade54c5f");
                        int position = holder.getAdapterPosition();
                        Address address = mList.get(position);
                        objectId = address.getObjectId();
                        Address newAddress = new Address();
                        newAddress.setObjectId(objectId);
                        newAddress.delete(new UpdateListener() {

                            @Override
                            public void done(BmobException e) {
                                if(e==null){
                                    Toast.makeText(view.getContext(), "删除成功", Toast.LENGTH_SHORT).show();
                                    ReceivingAddressActivity.receivingAddressActivity.find();
                                }else{
                                    Toast.makeText(view.getContext(), "删除失败，请联系管理员", Toast.LENGTH_SHORT).show();
                                }
                            }

                        });

                        ReceivingAddressActivity.receivingAddressActivity.find();
                        alertDialog.dismiss();
                    }
                });

                return true;
            }
        });



        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ReceivingAddressAdapter.ViewHolder viewHolder, int i) {
        Address address = mList.get(i);
        viewHolder.name.setText(address.getName());
        viewHolder.iphone.setText(address.getIphone());
        viewHolder.address.setText(address.getAddress());
        viewHolder.detailAddress.setText(address.getDetailedAddress());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
