package lzh.com.customeroilactivity.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import lzh.com.customeroilactivity.BaseActivity;
import lzh.com.customeroilactivity.Bean.Company;
import lzh.com.customeroilactivity.R;
import lzh.com.customeroilactivity.adapter.CompanyAdapter;

public class CompanyActivity extends BaseActivity {

    private ImageButton back;
    private TextView title;
    private ImageButton add;

    private List<Company> companyList = new ArrayList<>();
    private SharedPreferences sharedPreferences;
    private int userId;

    public static CompanyActivity companyActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);
        companyActivity = this;

        sharedPreferences = this.getSharedPreferences("userData",MODE_PRIVATE);
        userId = sharedPreferences.getInt("userId",0);

        find();
    }

    private void init(){
        back = (ImageButton)findViewById(R.id.top_left_btn);
        title = (TextView)findViewById(R.id.title_bar);
        add = (ImageButton)findViewById(R.id.add);

        title.setText("公司");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CompanyActivity.this,AddCompanyActivity.class);
                startActivity(intent);
            }
        });

    }

    private void adapterInit(){
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.list_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        CompanyAdapter companyAdapter = new CompanyAdapter(companyList);
        recyclerView.setAdapter(companyAdapter);
    }

    public void find(){
        BmobQuery<Company> bmobQuery = new BmobQuery<Company>();
        bmobQuery.addWhereEqualTo("userId",userId);
        bmobQuery.findObjects(new FindListener<Company>() {
            @Override
            public void done(List<Company> list, BmobException e) {
                if (e == null){
                    companyList.clear();
                    for (Company company:list){
                        companyList.add(company);
                    }
                    init();
                    adapterInit();
                }
            }
        });
    }

}
