package lzh.com.customeroilactivity.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import lzh.com.customeroilactivity.BaseActivity;
import lzh.com.customeroilactivity.LandActivity;
import lzh.com.customeroilactivity.R;

public class SetUpActivity extends BaseActivity {

    private ImageView back;
    private TextView out;
    private AlertDialog alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up);
        init();
    }

    private void init(){
        back = (ImageView)findViewById(R.id.top_left_btn);
        out = (TextView)findViewById(R.id.out);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View view1 = LayoutInflater.from(SetUpActivity.this).inflate(R.layout.out_dialog,null);
                TextView yes = (TextView)view1.findViewById(R.id.yes);
                TextView no = (TextView)view1.findViewById(R.id.no);
                TextView content = (TextView)view1.findViewById(R.id.content);

                content.setText("退出登陆后账号后需要重新登陆");
                AlertDialog dialog = new  AlertDialog.Builder(SetUpActivity.this,R.style.DialogStyle)
                        .setView(view1)
                        .create();
                dialog.show();
                alertDialog = dialog;

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finishAllActivity();
                        Intent intent = new Intent(SetUpActivity.this,LandActivity.class);
                        startActivity(intent);

                        SharedPreferences.Editor editor= getSharedPreferences("userData",MODE_PRIVATE).edit();
                        editor.clear();
                        editor.commit();

                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
            }
        });
    }
}
