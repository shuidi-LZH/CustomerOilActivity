package lzh.com.customeroilactivity.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import lzh.com.customeroilactivity.BaseActivity;
import lzh.com.customeroilactivity.R;

public class TicketActivity extends BaseActivity {

    private ImageButton back;
    private TextView title;
    private TextView companyName;
    private TextView ticketNumber;
    private TextView companyAddress;
    private TextView companyIphone;
    private TextView bankName;
    private TextView bankNumber;
    private Button go;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);
        init();

    }

    private void init(){
        back = (ImageButton) findViewById(R.id.top_left_btn);
        title = (TextView)findViewById(R.id.title_bar);
        companyName = (TextView)findViewById(R.id.company_name);
        ticketNumber = (TextView)findViewById(R.id.ticket_number);
        companyAddress = (TextView)findViewById(R.id.company_address);
        companyIphone = (TextView)findViewById(R.id.company_iphone);
        bankName = (TextView)findViewById(R.id.bank_name);
        bankNumber = (TextView)findViewById(R.id.bank_number);
        go = (Button)findViewById(R.id.go);

        companyName.setText(getIntent().getStringExtra("name"));
        title.setText("新增发票信息");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (companyName.getText().toString().length() == 0){
                    Toast.makeText(TicketActivity.this, "请输入公司抬头", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (ticketNumber.getText().toString().length() == 0){
                    Toast.makeText(TicketActivity.this, "请输入开票税号", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (companyAddress.getText().toString().length() == 0){
                    Toast.makeText(TicketActivity.this, "请输入注册地址", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (companyIphone.getText().toString().length() == 0){
                    Toast.makeText(TicketActivity.this, "请输入注册电话", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (bankName.getText().toString().length() == 0){
                    Toast.makeText(TicketActivity.this, "请输入开户行", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (bankNumber.getText().toString().length() == 0){
                    Toast.makeText(TicketActivity.this, "请输入银行账号", Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent();
                intent = intent.setClass(view.getContext(),TicketActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("companyName",companyName.getText().toString());
                bundle.putString("ticketNumber",ticketNumber.getText().toString());
                bundle.putString("companyAddress",companyAddress.getText().toString());
                bundle.putString("companyIphone",companyIphone.getText().toString());
                bundle.putString("bankName",bankName.getText().toString());
                bundle.putString("bankNumber",bankNumber.getText().toString());
                intent.putExtras(bundle);
                setResult(0,intent);//RESULT_OK是返回状态码 
                finish();
            }
        });
    }

}
