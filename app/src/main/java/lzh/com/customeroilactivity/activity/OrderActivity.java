package lzh.com.customeroilactivity.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import lzh.com.customeroilactivity.BaseActivity;
import lzh.com.customeroilactivity.Bean.BespeakRefual;
import lzh.com.customeroilactivity.R;
import lzh.com.customeroilactivity.adapter.OrderAdapter;

public class OrderActivity extends BaseActivity {

    private ImageButton back;
    private TextView title;
    private RecyclerView recyclerView;
    private int userId;

    private List<BespeakRefual> bespeakRefualList = new ArrayList<>();
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        sharedPreferences = this.getSharedPreferences("userData",MODE_PRIVATE);
        userId = sharedPreferences.getInt("userId",0);
        find();
    }

    private void init(){
        back = (ImageButton)findViewById(R.id.top_left_btn);
        title = (TextView)findViewById(R.id.title_bar);
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);

        title.setText("我的订单");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        OrderAdapter orderAdapter = new OrderAdapter(bespeakRefualList);
        recyclerView.setAdapter(orderAdapter);
    }

    public void find(){
        BmobQuery<BespeakRefual> bmobQuery = new BmobQuery<BespeakRefual>();
        bmobQuery.addWhereEqualTo("userId",userId);
        bmobQuery.findObjects(new FindListener<BespeakRefual>() {
            @Override
            public void done(List<BespeakRefual> list, BmobException e) {
                if (e == null){
                    bespeakRefualList.clear();
                    for (BespeakRefual bespeakRefual:list){
                        bespeakRefualList.add(bespeakRefual);
                    }
                    init();
                }
            }
        });
    }

}
