package lzh.com.customeroilactivity.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.UpdateListener;
import lzh.com.customeroilactivity.BaseActivity;
import lzh.com.customeroilactivity.Bean.Person;
import lzh.com.customeroilactivity.R;

public class ModifyNameActivity extends BaseActivity {

    private ImageButton back;
    private TextView title;
    private EditText name;
    private RadioButton man;
    private RadioButton woman;
    private TextView iphone;
    private Button go;

    private SharedPreferences sharedPreferences;
    private int sexNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_name);
        sharedPreferences = this.getSharedPreferences("userData",MODE_PRIVATE);
        init();

        if (sharedPreferences.getInt("sex",0) == 1){
            man.setChecked(true);
        }else {
            woman.setChecked(true);
        }

        man.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    sexNumber = 1;
                }
            }
        });

        woman.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    sexNumber = 2;
                }
            }
        });
    }

    private void init(){
        back = (ImageButton)findViewById(R.id.top_left_btn);
        title = (TextView)findViewById(R.id.title_bar);
        name = (EditText)findViewById(R.id.name);
        man = (RadioButton)findViewById(R.id.man);
        woman = (RadioButton)findViewById(R.id.woman);
        iphone = (TextView)findViewById(R.id.iphone);
        go = (Button)findViewById(R.id.go);

        title.setText("修改个人信息");
        name.setText(sharedPreferences.getString("name",""));
        name.setSelection(name.getText().toString().length());
        iphone.setText(sharedPreferences.getString("iphone",""));

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Person person = new Person();
                person.setName(name.getText().toString());
                person.setRoleId(sharedPreferences.getInt("roleId",0));
                person.setSex(sexNumber);
                person.setUserId(sharedPreferences.getInt("userId",0));
                person.update(sharedPreferences.getString("objectId",""), new UpdateListener() {

                    @Override
                    public void done(BmobException e) {
                        if(e==null){
                            finish();
                            SharedPreferences.Editor editor= getSharedPreferences("userData",MODE_PRIVATE).edit();
                            editor.putString("name",name.getText().toString());
                            editor.putInt("sex",sexNumber);
                            editor.commit();
                            PersonalActivity.personalActivity.secondInit();
                            Toast.makeText(ModifyNameActivity.this, "修改成功", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(ModifyNameActivity.this, "修改失败，请联系管理员", Toast.LENGTH_SHORT).show();
                        }
                    }

                });
            }
        });


    }

}
