package lzh.com.customeroilactivity.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import lzh.com.customeroilactivity.BaseActivity;
import lzh.com.customeroilactivity.Bean.Address;
import lzh.com.customeroilactivity.R;
import lzh.com.customeroilactivity.adapter.ReceivingAddressAdapter;

public class ReceivingAddressActivity extends BaseActivity {

    private List<Address> addressList = new ArrayList<>();
    private SharedPreferences sharedPreferences;
    private int userId;
    public static ReceivingAddressActivity receivingAddressActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiving_address);
        receivingAddressActivity = this;
        Bmob.initialize(this, "5e509d9bd6152b90a5e72d4cade54c5f");
        sharedPreferences = this.getSharedPreferences("userData",MODE_PRIVATE);
        userId = sharedPreferences.getInt("userId",0);
        find();

    }

    private void init(){
        ImageButton back = (ImageButton)findViewById(R.id.top_left_btn);
        TextView title = (TextView)findViewById(R.id.title_bar);
        ImageButton add = (ImageButton)findViewById(R.id.add);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        title.setText("添加地址");

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ReceivingAddressActivity.this,AddAdressActivity.class);
                startActivity(intent);
            }
        });

    }

    private void adapterInit(){
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.list_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        ReceivingAddressAdapter receivingAddressAdapter = new ReceivingAddressAdapter(addressList);
        recyclerView.setAdapter(receivingAddressAdapter);
    }

    public void find(){
        BmobQuery<Address> bmobQuery = new BmobQuery<Address>();
        bmobQuery.addWhereEqualTo("userId",userId);
        bmobQuery.findObjects(new FindListener<Address>() {
            @Override
            public void done(List<Address> list, BmobException e) {
                if (e == null){
                    addressList.clear();
                    for (Address address:list){
                        addressList.add(address);
                    }
                    init();
                    adapterInit();
                }
            }
        });
    }

}
