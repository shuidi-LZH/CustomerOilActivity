package lzh.com.customeroilactivity.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import lzh.com.customeroilactivity.BaseActivity;
import lzh.com.customeroilactivity.R;

public class PersonalActivity extends BaseActivity {

    private ImageView back;
    private ImageView setUp;
    private ImageView order;
    private TextView vehicle;
    private TextView company;

    private TextView name;
    private TextView iphone;
    private ImageButton modifyName;
    private RelativeLayout addressManagement;
    private RelativeLayout companyManagement;

    private SharedPreferences sharedPreferences;
    public static PersonalActivity personalActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        personalActivity = this;
        sharedPreferences = this.getSharedPreferences("userData",MODE_PRIVATE);

        if (sharedPreferences.getInt("roleId",0) == 3){
            setContentView(R.layout.activity_personal);
            init();
        }else if (sharedPreferences.getInt("roleId",0) == 2){
            setContentView(R.layout.second_activity_personal);
            secondInit();
        }

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PersonalActivity.this,OrderActivity.class);
                startActivity(intent);
            }
        });

    }

    private void init(){
        back = (ImageView)findViewById(R.id.top_left_btn);
        setUp = (ImageView)findViewById(R.id.set_up);
        order = (ImageView)findViewById(R.id.order);
        vehicle = (TextView)findViewById(R.id.vehicle);
        company = (TextView)findViewById(R.id.company);

        vehicle.setText(sharedPreferences.getString("vehicleNumber",""));
        company.setText(sharedPreferences.getString("companyName",""));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        setUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PersonalActivity.this,SetUpActivity.class);
                startActivity(intent);
            }
        });
    }

    protected void secondInit(){
        back = (ImageView)findViewById(R.id.top_left_btn);
        setUp = (ImageView)findViewById(R.id.set_up);
        order = (ImageView)findViewById(R.id.order);
        name = (TextView)findViewById(R.id.name);
        iphone = (TextView)findViewById(R.id.iphone);
        modifyName = (ImageButton)findViewById(R.id.modify_name);
        addressManagement = (RelativeLayout)findViewById(R.id.address);
        companyManagement = (RelativeLayout)findViewById(R.id.company_management);

        name.setText(sharedPreferences.getString("name",""));
        iphone.setText(sharedPreferences.getString("iphone",""));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        setUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PersonalActivity.this,SetUpActivity.class);
                startActivity(intent);
            }
        });

        modifyName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PersonalActivity.this,ModifyNameActivity.class);
                startActivity(intent);
            }
        });

        addressManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PersonalActivity.this,ReceivingAddressActivity.class);
                startActivity(intent);
            }
        });

        companyManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PersonalActivity.this,CompanyActivity.class);
                startActivity(intent);
            }
        });
    }
}
