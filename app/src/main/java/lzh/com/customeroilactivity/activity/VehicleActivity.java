package lzh.com.customeroilactivity.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import lzh.com.customeroilactivity.BaseActivity;
import lzh.com.customeroilactivity.Bean.Person;
import lzh.com.customeroilactivity.LandActivity;
import lzh.com.customeroilactivity.MainActivity;
import lzh.com.customeroilactivity.R;

public class VehicleActivity extends BaseActivity {

    private ImageButton back;
    private TextView number;
    private TextView password;
    private TextView go;
    private TextView textView;
    private int roleId;
    private boolean stye = false;
    private SharedPreferences sharedPreferences;
    private int goId = 0;
    private String objectId;
    private int userId;
    private String name;
    private String iphone;
    private String vehicleNumber;
    private String companyName;
    private String companyAddress;
    private String vehiclepassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle);

        Bmob.initialize(this, "5e509d9bd6152b90a5e72d4cade54c5f");

        sharedPreferences = VehicleActivity.this.getSharedPreferences("userData",MODE_PRIVATE);
        if (!"" .equals(sharedPreferences.getString("objectId", "") )){
            goId = 1;
        }

        if (goId == 1){
            String str=sharedPreferences.getString("objectId", " ");
            Log.d("ok","objectId:"+str);
            Intent intent = new Intent(VehicleActivity.this,MainActivity.class);
            intent.putExtra("roleId",3+"");
            startActivity(intent);
            finish();
            LandActivity.landActivity.finish();
        }

        init();

    }

    private void init(){
        back = (ImageButton)findViewById(R.id.top_left_btn);
        number = (TextView)findViewById(R.id.number);
        password = (TextView)findViewById(R.id.password);
        go = (TextView)findViewById(R.id.go);
        textView = (TextView)findViewById(R.id.tv);

        textView.setText("密"+"\u3000"+"码：");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (goId == 0){
                    if (number.length()==0){
                        Toast.makeText(VehicleActivity.this, "请输入账号", Toast.LENGTH_SHORT).show();
                    }
                    if (password.length()==0){
                        Toast.makeText(VehicleActivity.this, "请输入密码", Toast.LENGTH_SHORT).show();
                    }
                    find();
                }

            }
        });
    }

    private void find(){
        BmobQuery<Person> query = new BmobQuery<Person>();

        query.addWhereEqualTo("vehicleNumber", number.getText()+"");
        //执行查询方法
        query.findObjects(new FindListener<Person>() {
            @Override
            public void done(List<Person> object, BmobException e) {
                if(e==null){
                    //toast("查询成功：共"+object.size()+"条数据。");
                    for (Person person : object) {
                        if (person.getVehiclepassword().equals(password.getText()+"")){
                            stye = true;
                        }
                        //获得RoleId的信息
                        roleId = 3;
                        objectId = person.getObjectId();
                        userId = person.getUserId();
                        name = person.getName();
                        iphone = person.getIphone();
                        companyName = person.getCompanyName();
                        companyAddress = person.getCompanyAddress();
                        vehicleNumber = person.getVehicleNumber();
                        vehiclepassword = person.getVehiclepassword();
                        if (stye){
                            Intent intent = new Intent(VehicleActivity.this,MainActivity.class);
                            intent.putExtra("roleId",3+"");
                            startActivity(intent);
                            clearData();
                            saved(roleId,objectId,userId,name,iphone,companyName,companyAddress,vehicleNumber,vehiclepassword);
                            finish();
                            LandActivity.landActivity.finish();

                        }else {
                            Toast.makeText(VehicleActivity.this, "账号或密码不对，若未开通，请联系管理员", Toast.LENGTH_SHORT).show();
                        }
                    }

                }else{
                    Log.i("bmob","失败："+e.getMessage()+","+e.getErrorCode());
                }
            }
        });
    }

    private void saved(int roleId,String objectId,int userId,String name,String iphone, String companyName, String companyAddress, String vehicleNumber,String vehiclepassword){
        SharedPreferences.Editor editor= getSharedPreferences("userData",MODE_PRIVATE).edit();
        editor.putInt("roleId",roleId);
        editor.putString("vehicleNumber",vehicleNumber);
        editor.putString("vehiclepassword",vehiclepassword);
        editor.putString("objectId",objectId);
        editor.putInt("userId",userId);
        editor.putString("name",name);
        editor.putString("iphone",iphone);
        editor.putString("companyName",companyName);
        editor.putString("companyAddress",companyAddress);
        editor.commit();
    }

    private void clearData(){
        SharedPreferences.Editor editor= getSharedPreferences("userData",MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();

    }


}
