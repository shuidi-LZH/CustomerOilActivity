package lzh.com.customeroilactivity.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import lzh.com.customeroilactivity.BaseActivity;
import lzh.com.customeroilactivity.R;

public class NewsActivity extends BaseActivity {

    private ImageButton back;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        title = (TextView)findViewById(R.id.title_bar);
        back = (ImageButton)findViewById(R.id.top_left_btn);

        title.setText("消息");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
