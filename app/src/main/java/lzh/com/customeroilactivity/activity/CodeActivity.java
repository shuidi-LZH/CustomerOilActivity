package lzh.com.customeroilactivity.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.king.zxing.util.CodeUtils;

import lzh.com.customeroilactivity.BaseActivity;
import lzh.com.customeroilactivity.R;

public class CodeActivity extends BaseActivity {

    private ImageView ivCode;
    private String content;
    private ImageButton back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code);
        back = (ImageButton)findViewById(R.id.top_left_btn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ivCode = findViewById(R.id.ivCode);
        content = getIntent().getStringExtra("content");
        createQRCode(content);
    }

    /**
     * 生成二维码
     * @param content
     */
    private void createQRCode(String content){
        //生成二维码最好放子线程生成防止阻塞UI，这里只是演示
        Bitmap logo = BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher);
        Bitmap bitmap =  CodeUtils.createQRCode(content,600,logo);
        //显示二维码
        ivCode.setImageBitmap(bitmap);
    }
}
