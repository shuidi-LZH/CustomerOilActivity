package lzh.com.customeroilactivity.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import cn.bmob.v3.Bmob;
import lzh.com.customeroilactivity.BaseActivity;
import lzh.com.customeroilactivity.LandActivity;
import lzh.com.customeroilactivity.MainActivity;
import lzh.com.customeroilactivity.R;

public class AdvertisementActivity extends BaseActivity {

    private ImageView all;
    private TextView count;
    private SharedPreferences sharedPreferences;
    private int goId = 0;
    private int goToId = 0;
    private int roleId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisement);

        all = (ImageView) findViewById(R.id.all);
        count = (TextView)findViewById(R.id.count);
        Glide.with(this).load(R.drawable.first_img).into(all);

        init();
    }

    private void init(){
        new CountDownTimer(3*1000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //倒计时的过程中回调该函数
                count.setText(millisUntilFinished/1000+1+"");
            }

            @Override
            public void onFinish() {
                //倒计时结束时回调该函数

                sharedPreferences = AdvertisementActivity.this.getSharedPreferences("userData",MODE_PRIVATE);

                if (!"" .equals(sharedPreferences.getString("objectId", "") )){
                    goId = 1;
                }

                if (goId == 1){
                    int roleId=sharedPreferences.getInt("roleId",0);
                    Intent intent = new Intent(AdvertisementActivity.this,MainActivity.class);
                    intent.putExtra("roleId",roleId+"");
                    startActivity(intent);
                    finish();

                }else {
                    Intent intent = new Intent(AdvertisementActivity.this,LandActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }.start();
    }

}
