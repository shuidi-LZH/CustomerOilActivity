package lzh.com.customeroilactivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import lzh.com.customeroilactivity.activity.CodeActivity;
import lzh.com.customeroilactivity.activity.NewsActivity;
import lzh.com.customeroilactivity.activity.PersonalActivity;
import lzh.com.customeroilactivity.activity.RefuelActivity;

public class MainActivity extends BaseActivity {

    private ImageView img1;
    private ImageView img2;
    private ImageView img3;
    private ImageView round;
    private ImageView news;
    private String content;

    private String roleId;
    private RequestManager glideRequest;

    //退出时的时间
    private long mExitTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //CrashReport.initCrashReport(this, "c9f09fde31", false);
        roleId = getIntent().getStringExtra("roleId");

        if (roleId.equals("2")){
            setContentView(R.layout.activity_main);
            init();
        }else {
            setContentView(R.layout.second_main);
            secondInit();
        }

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences("userData",MODE_PRIVATE);
                content = sharedPreferences.getString("objectId","");
                Intent intent = new Intent(MainActivity.this,CodeActivity.class);
                intent.putExtra("content",content);
                startActivity(intent);
            }
        });

        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,NewsActivity.class);
                startActivity(intent);
            }
        });

        round.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,PersonalActivity.class);
                startActivity(intent);
            }
        });

    }

    private void init(){
        img1 = (ImageView) findViewById(R.id.img_bespoke);
        img2 = (ImageView)findViewById(R.id.img_qrcode);
        img3 = (ImageView)findViewById(R.id.img_mailbox);
        round = (ImageView) findViewById(R.id.personal);
        news = (ImageView)findViewById(R.id.news);

        glideRequest = Glide.with(MainActivity.this);
        glideRequest.load(R.drawable.order).transform(new GlideRoundTransform(MainActivity.this, 15)).into(img1);
        glideRequest.load(R.drawable.order).transform(new GlideRoundTransform(MainActivity.this, 15)).into(img2);
        glideRequest.load(R.drawable.order).transform(new GlideRoundTransform(MainActivity.this, 15)).into(img3);
        glideRequest.load(R.drawable.news).transform(new GlideRoundTransform(MainActivity.this, 15)).into(news);

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,RefuelActivity.class);
                startActivity(intent);
            }
        });
    }

    private void secondInit(){
        img2 = (ImageView)findViewById(R.id.img_qrcode);
        img3 = (ImageView)findViewById(R.id.img_mailbox);
        round = (ImageView) findViewById(R.id.personal);
        news = (ImageView)findViewById(R.id.news);

        glideRequest = Glide.with(MainActivity.this);
        glideRequest.load(R.drawable.order).transform(new GlideRoundTransform(MainActivity.this, 15)).into(img2);
        glideRequest.load(R.drawable.order).transform(new GlideRoundTransform(MainActivity.this, 15)).into(img3);
        glideRequest.load(R.drawable.news).transform(new GlideRoundTransform(MainActivity.this, 15)).into(news);

    }


    //对返回键进行监听
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

            exit();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void exit() {
        if ((System.currentTimeMillis() - mExitTime) > 2000) {
            Toast.makeText(MainActivity.this, "再按一次退出加油", Toast.LENGTH_SHORT).show();
            mExitTime = System.currentTimeMillis();
        } else {
            finish();
            System.exit(0);
        }
    }
}
