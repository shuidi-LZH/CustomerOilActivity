package lzh.com.customeroilactivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import lzh.com.customeroilactivity.activity.VehicleActivity;

public class LandActivity extends BaseActivity {

    private TextView vehicle;
    private TextView iphone;
    private TextView goIphone;
    public static LandActivity landActivity;
    private AlertDialog alertDialog;
    //退出时的时间
    private long mExitTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_land);
        landActivity = this;
        init();
    }




    public void init(){
        vehicle = (TextView)findViewById(R.id.vehicle);
        iphone = (TextView)findViewById(R.id.iphone);
        goIphone = (TextView)findViewById(R.id.go_iphone);

        iphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LandActivity.this,SecondLandActivity.class);
                startActivity(intent);
            }
        });

        vehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LandActivity.this,VehicleActivity.class);
                startActivity(intent);
            }
        });

        goIphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View view1 = LayoutInflater.from(LandActivity.this).inflate(R.layout.out_dialog,null);
                TextView yes = (TextView)view1.findViewById(R.id.yes);
                TextView no = (TextView)view1.findViewById(R.id.no);
                TextView content = (TextView)view1.findViewById(R.id.content);

                content.setText("拨打王经理电话获得商务合作"+"17756511917");
                AlertDialog dialog = new  AlertDialog.Builder(LandActivity.this,R.style.DialogStyle)
                        .setView(view1)
                        .create();
                dialog.show();
                alertDialog = dialog;

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ContextCompat.checkSelfPermission(LandActivity.this, Manifest.permission.CALL_PHONE)!= PackageManager.PERMISSION_GRANTED){
                            //如果不同意，就去请求权限   参数1：上下文，2：权限，3：请求码
                            ActivityCompat.requestPermissions(LandActivity.this,new String []{Manifest.permission.CALL_PHONE},1);
                        }else {
                            //同意就拨打
                            call();
                        }
                        alertDialog.dismiss();
                    }
                });
            }
        });
    }

    private void call() {
        Intent intent=new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:17756511917"));
        try {
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //判断请求码
        switch (requestCode){
            case 1:
                //如果同意，就拨打
                if (grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    call();
                }else{
                    Toast.makeText(this,"请同意拨打电话的权限",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    //对返回键进行监听
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {

            exit();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void exit() {
        if ((System.currentTimeMillis() - mExitTime) > 2000) {
            Toast.makeText(LandActivity.this, "再按一次退出加油", Toast.LENGTH_SHORT).show();
            mExitTime = System.currentTimeMillis();
        } else {
            finish();
            System.exit(0);
        }
    }

}
