package lzh.com.customeroilactivity.Bean;

import cn.bmob.v3.BmobObject;

/**
 * Created by LuoZhihao on 2018/10/29.
 */
public class Company extends BmobObject {

    private int userId;
    private String companyDetailName;
    private String companyName;
    private String person;
    private String industry;
    private String money;
    private String address;
    private String detailAddress;
    private String remarks;
    private String companyNumber;
    private String companyAddress;
    private String companyIphone;
    private String bankName;
    private String bankNumber;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyDetailName() {
        return companyDetailName;
    }

    public void setCompanyDetailName(String companyDetailName) {
        this.companyDetailName = companyDetailName;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyNumber() {
        return companyNumber;
    }

    public void setCompanyNumber(String companyNumber) {
        this.companyNumber = companyNumber;
    }

    public String getCompanyIphone() {
        return companyIphone;
    }

    public void setCompanyIphone(String companyIphone) {
        this.companyIphone = companyIphone;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankNumber() {
        return bankNumber;
    }

    public void setBankNumber(String bankNumber) {
        this.bankNumber = bankNumber;
    }
}
