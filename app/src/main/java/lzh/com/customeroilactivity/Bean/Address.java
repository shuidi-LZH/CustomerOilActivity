package lzh.com.customeroilactivity.Bean;

import cn.bmob.v3.BmobObject;

/**
 * Created by LuoZhihao on 2018/10/26.
 */
public class Address extends BmobObject {

    private int userId;
    private String name;
    private String iphone;
    private String address;
    private String detailedAddress;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIphone() {
        return iphone;
    }

    public void setIphone(String iphone) {
        this.iphone = iphone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDetailedAddress() {
        return detailedAddress;
    }

    public void setDetailedAddress(String detailedAddress) {
        this.detailedAddress = detailedAddress;
    }
}
