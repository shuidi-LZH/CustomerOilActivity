package lzh.com.customeroilactivity.Bean;

import cn.bmob.v3.BmobObject;

/**
 * Created by LuoZhihao on 2018/9/30.
 */
public class Person extends BmobObject {

    private int roleId;
    private int userId;
    private int sex;
    private String name;
    private String iphone;
    private String password;
    private String vehicleNumber;
    private String companyName;
    private String companyAddress;
    private String vehiclepassword;

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getVehiclepassword() {
        return vehiclepassword;
    }

    public void setVehiclepassword(String vehiclepassword) {
        this.vehiclepassword = vehiclepassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getIphone() {
        return iphone;
    }

    public void setIphone(String iphone) {
        this.iphone = iphone;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }


    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }
}