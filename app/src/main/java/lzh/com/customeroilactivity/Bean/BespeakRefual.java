package lzh.com.customeroilactivity.Bean;

import cn.bmob.v3.BmobObject;

/**
 * Created by LuoZhihao on 2018/10/30.
 */
public class BespeakRefual extends BmobObject {

    private String bespeakRefualId;
    private String state;
    private int userId;
    private String name;
    private String iphone;
    private String money;
    private String companyAddress;
    private String time;
    private String companyName;
    private String oilType;
    private String quantity;
    private String mode;
    private String equipment;
    private String remarks;

    public String getBespeakRefualId() {
        return bespeakRefualId;
    }

    public void setBespeakRefualId(String bespeakRefualId) {
        this.bespeakRefualId = bespeakRefualId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public String getRemarks() {
        return remarks;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getIphone() {
        return iphone;
    }

    public String getName() {
        return name;
    }





    public String getEquipment() {
        return equipment;
    }

    public String getMode() {
        return mode;
    }

    public String getOilType() {
        return oilType;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getTime() {
        return time;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setIphone(String iphone) {
        this.iphone = iphone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public void setOilType(String oilType) {
        this.oilType = oilType;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setTime(String time) {
        this.time = time;
    }


}
