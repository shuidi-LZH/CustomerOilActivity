package lzh.com.customeroilactivity.util;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by wangqile on 2018/3/29.
 * 时间选择机器
 */

public class TImeTickerUtils {
    public static TImeTickerUtils getInstance() {
        TImeTickerUtils tImeTickerUtils=new TImeTickerUtils();
        return tImeTickerUtils;
    }

    public void initTheDatePickers(Context context ,TextView textViews){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.CHINA);
        String now = sdf.format(new Date());
        String newNow = now.substring(0,11);
        CustomDatePicker customDatePicker2 = new CustomDatePicker(context, (CustomDatePicker.ResultHandler) time -> { // 回调接口，获得选中的时间
           textViews.setText(time);

        }, newNow+"00:00", "2100-12-31 23:59"); // 初始化日期格式请用：yyyy-MM-dd HH:mm，否则不能正常运行
        customDatePicker2.showSpecificTime(true); // 显示时和分
        customDatePicker2.setIsLoop(false); // 允许循环滚动
        customDatePicker2.hour_pv.setVisibility(View.VISIBLE);
        customDatePicker2.minute_pv.setVisibility(View.VISIBLE);
        customDatePicker2.hour_text.setVisibility(View.VISIBLE);
        customDatePicker2.show(now);
    }

}
