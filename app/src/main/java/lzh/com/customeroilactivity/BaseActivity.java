package lzh.com.customeroilactivity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.tencent.bugly.crashreport.CrashReport;

import java.util.ArrayList;

import cn.bmob.v3.Bmob;

/**
 * Created by LuoZhihao on 2018/10/23.
 */
public class BaseActivity extends AppCompatActivity {


    public static ArrayList<Activity> mActivityList = new ArrayList<Activity>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBugly();

        Bmob.initialize(this, "5e509d9bd6152b90a5e72d4cade54c5f");

        //CrashReport.initCrashReport(this, "c9f09fde31", false);
        if (!mActivityList.contains(this)){
            mActivityList.add(this);
            Log.d("正在加入的Activity",this.getClass().getCanonicalName());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivityList.remove(this);
        Log.d("正在移除的Activity",this.getClass().getCanonicalName());
    }

    /**
     * 关闭所有Activity
     */
    public static void finishAllActivity(){
        for (Activity activity : mActivityList){
            if (!activity.isFinishing()){
                activity.finish();
            }
        }
    }

    /**
     * 初始化腾讯bug管理平台
     */
    private void initBugly() {
        /* Bugly SDK初始化
         * 参数1：上下文对象
         * 参数2：APPID，平台注册时得到,注意替换成你的appId
         * 参数3：是否开启调试模式，调试模式下会输出'CrashReport'tag的日志
         * 注意：如果您之前使用过Bugly SDK，请将以下这句注释掉。
         */
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(getApplicationContext());
        strategy.setAppVersion(getApplicationContext().getPackageName());
        strategy.setAppReportDelay(2000);                          //Bugly会在启动2s后联网同步数据

        /*  第三个参数为SDK调试模式开关，调试模式的行为特性如下：
            输出详细的Bugly SDK的Log；
            每一条Crash都会被立即上报；
            自定义日志将会在Logcat中输出。
            建议在测试阶段建议设置成true，发布时设置为false。*/

        CrashReport.initCrashReport(getApplicationContext(), "c9f09fde31", true ,strategy);

        //Bugly.init(getApplicationContext(), "1374455732", false);
    }
}
